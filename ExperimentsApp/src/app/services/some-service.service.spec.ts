import { TestBed, inject } from '@angular/core/testing';

import { SomeServiceService } from './some-service.service';

describe('SomeServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SomeServiceService]
    });
  });

  it('should be created', inject([SomeServiceService], (service: SomeServiceService) => {
    expect(service).toBeTruthy();
  }));
});
