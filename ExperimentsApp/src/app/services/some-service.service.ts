import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SomeServiceService {

  constructor(private http: HttpClient) { }

  callCloudFunction(stylistsArr, timeStart) {
    const params = new HttpParams()
      .set('stylistsArr', JSON.stringify(stylistsArr))
      .set('date', '2018-09-04')
      .set('startTime', timeStart);
      
    return this.http.get('https://us-central1-demodk3.cloudfunctions.net/freeBusyFlow2',
      { params });
  }
}
