import { Component } from '@angular/core';
import * as $ from 'jquery';
import {Sort} from '@angular/material';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';

import { of } from 'rxjs';

//declare var $: any;
export interface Dessert {
  calories: number;
  carbs: number;
  fat: number;
  name: string;
  protein: number;
}


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'app';
  sticky: number;

  desserts: Dessert[] = [
    {name: 'Frozen yogurt', calories: 159, fat: 6, carbs: 24, protein: 4},
    {name: 'Ice cream sandwich', calories: 237, fat: 9, carbs: 37, protein: 4},
    {name: 'Eclair', calories: 262, fat: 16, carbs: 24, protein: 6},
    {name: 'Cupcake', calories: 305, fat: 4, carbs: 67, protein: 4},
    {name: 'Gingerbread', calories: 356, fat: 16, carbs: 49, protein: 4},
  ];

  constructor() {
       
  }

  ngOnInit(): void {
    let pos = $('.navbar').position().top;
    this.sticky = pos;

    console.log(this.sticky);
  }

  isSticky(){
    if(window.pageYOffset >= this.sticky)
       $('.navbar').addClass('sticky');
       //return true;
    else{
       $('.navbar').removeClass('sticky');
       //return false;  
    }      
    
  }

  
}


