import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page3',
  templateUrl: './page3.component.html',
  styleUrls: ['./page3.component.css']
})
export class Page3Component implements OnInit {

  days: any[] = [
    { value: '2', viewValue: '2 days' },
    { value: '5', viewValue: '5 days' },
    { value: '10', viewValue: '10 days' }
  ];

  day: number;
  eventArr: any[] = [
    {
      'name': 'Event2',
      'date': '2018-07-11',
      'startDate': '08:00',
      'endDate': '08:30',
      'duration': 30,
      'stylist': '1rr4cb'
    },
    {
      'name': 'Event3',
      'date': '2018-07-14',
      'startDate': '10:20',
      'endDate': '10:30',
      'duration': 10,
      'stylist': 'cc44mv'
    },
    {
      'name': 'Event4',
      'date': '2018-07-17',
      'startDate': '10:10',
      'endDate': '10:45',
      'duration': 35,
      'stylist': 'cc44mv'
    },
    {
      'name': 'Event5',
      'date': '2018-07-15',
      'startDate': '11:20',
      'endDate': '11:50',
      'duration': 30,
      'stylist': '1rr4cb'
    },
    {
      'name': 'Event6',
      'date': '2018-07-12',
      'startDate': '11:20',
      'endDate': '11:40',
      'duration': 20,
      'stylist': 'cc44mv'
    },
    {
      'name': 'Event7',
      'date': '2018-07-11',
      'startDate': '08:05',
      'endDate': '08:40',
      'duration': 35,
      'stylist': 'cc44mv'
    },
    {
      'name': 'Event8',
      'date': '2018-07-27',
      'startDate': '08:05',
      'endDate': '08:15',
      'duration': 10,
      'stylist': '1234cv'
    },
    {
      'name': 'Event25',
      'date': '2018-07-23',
      'startDate': '13:30',
      'endDate': '13:50',
      'duration': 20,
      'stylist': '1234cv'
    },
    {
      'name': 'Event27',
      'date': '2018-07-22',
      'startDate': '13:35',
      'endDate': '13:45',
      'duration': 10,
      'stylist': '1234cv'
    },

    {
      'name': 'Event26',
      'date': '2018-07-12',
      'startDate': '16:00',
      'endDate': '16:10',
      'duration': 10,
      'stylist': '1234cv'
    },
    {
      'name': 'Event9',
      'date': '2018-07-13',
      'startDate': '11:25',
      'endDate': '11:55',
      'duration': 30,
      'stylist': '1rr4cb'
    },
    {
      'name': 'Julia Event10',
      'date': '2018-07-11',
      'startDate': '09:10',
      'endDate': '09:40',
      'duration': 30,
      'stylist': '1234cv'
    },
    {
      'name': 'Event11 Ruser',
      'date': '2018-07-13',
      'startDate': '09:40',
      'endDate': '10:00',
      'duration': 20,
      'stylist': '1234cv'
    },

    {
      'name': 'Event14 Ruser',
      'date': '2018-07-18',
      'startDate': '09:00',
      'endDate': '10:55',
      'duration': 115,
      'stylist': '1234cv'
    },
    {
      'name': 'Event15 Ruser',
      'date': '2018-07-19',
      'startDate': '12:10',
      'endDate': '12:25',
      'duration': 15,
      'stylist': '1234cv'
    },
    {
      'name': 'Event16',
      'date': '2018-07-19',
      'startDate': '08:00',
      'endDate': '08:10',
      'duration': 10,
      'stylist': '1234cv'
    },
    {
      'name': 'Event1',
      'date': '2018-07-26',
      'startDate': '08:05',
      'endDate': '08:15',
      'duration': 10,
      'stylist': 'cc44mv'
    },
    {
      'name': 'Someone Event17',
      'date': '2018-07-27',
      'startDate': '08:55',
      'endDate': '10:55',
      'duration': 120,
      'stylist': '1rr4cb'
    },
    {
      'name': 'Event18 Sam Ruser',
      'date': '2018-07-28',
      'startDate': '09:10',
      'endDate': '09:40',
      'duration': 30,
      'stylist': '1rr4cb'
    },
    {
      'name': 'Event19',
      'date': '2018-07-22',
      'startDate': '09:40',
      'endDate': '10:00',
      'duration': 20,
      'stylist': '1rr4cb'
    },
    {
      'name': 'Sam Event20',
      'date': '2018-07-28',
      'startDate': '12:20',
      'endDate': '12:45',
      'duration': 25,
      'stylist': '1234cv'
    },
    {
      'name': 'Sam Event21',
      'date': '2018-07-30',
      'startDate': '14:20',
      'endDate': '14:45',
      'duration': 25,
      'stylist': '1234cv'
    },
    {
      'name': 'Event22 Ruser',
      'date': '2018-07-30',
      'startDate': '12:40',
      'endDate': '13:00',
      'duration': 20,
      'stylist': '1234cv'
    },

    {
      'name': 'Event24',
      'date': '2018-07-25',
      'startDate': '14:15',
      'endDate': '14:30',
      'duration': 15,
      'stylist': '1234cv'
    },

    {
      'name': 'Event23',
      'date': '2018-07-24',
      'startDate': '14:10',
      'endDate': '14:20',
      'duration': 10,
      'stylist': '1234cv'
    },
    {
      'name': 'Last28',
      'date': '2018-07-22',
      'startDate': '14:10',
      'endDate': '14:20',
      'duration': 10,
      'stylist': 'bb34cv'
    },
    {
      'name': 'Event12 Sam Ruser',
      'date': '2018-08-2',
      'startDate': '17:00',
      'endDate': '17:15',
      'duration': 15,
      'stylist': '1234cv'
    },
    {
      'name': 'Sam Event13',
      'date': '2018-08-1',
      'startDate': '17:00',
      'endDate': '18:05',
      'duration': 65,
      'stylist': '1234cv'
    },
    {
      'name': 'Event1',
      'date': '2018-07-13',
      'startDate': '08:00',
      'endDate': '08:55',
      'duration': 55,
      'stylist': '1rr4cb'
    },
  ];

  sortedData: any[];

  constructor() { }

  ngOnInit() {
    this.sortedData = this.eventArr.slice();
  }

  sortData(sort: any) {
    console.log(sort);
    const data = this.eventArr.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.sortedData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'name': return compare(a.name, b.name, isAsc);
        case 'startDate': return compare(a.startDate, b.startDate, isAsc);
        case 'endDate': return compare(a.endDate, b.endDate, isAsc);
        case 'duration': return compare(a.duration, b.duration, isAsc);
        case 'stylist': return compare(a.stylist, b.stylist, isAsc);
        default: return 0;
      }
    });
  }

  deleteBookings(days) {
    //console.log(days);
    var eventCounter = 0;
    var date = new Date();
    for (let i = 0; i < this.eventArr.length; i++) {
      let yr = this.eventArr[i].date.substr(0, 4);
      let mon = this.eventArr[i].date.substr(5, 2);
      let day = this.eventArr[i].date.substr(8, 2);

      let nDate = new Date(yr, mon - 1, day)

      var subDays: number = (date.getTime() - nDate.getTime()) / (86400000);
      //console.log(date , nDate, subDays);

      console.log(subDays + ">" + days);
      if (subDays > days) {
        //console.log(this.eventArr[i]);
        eventCounter++;
      }
    }
    console.log("Total: " + this.eventArr.length);
    console.log("Deleted: " + eventCounter);
    console.log("Left: ", this.eventArr.length - eventCounter);


  }
}

function compare(a, b, isAsc) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
