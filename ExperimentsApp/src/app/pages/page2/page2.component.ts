import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-page2',
  templateUrl: './page2.component.html',
  styleUrls: ['./page2.component.css']
})
export class Page2Component implements OnInit {
  animals: any[] = [
    {
      id: "123",
      name: "Cat"
    },
    {
      id: "abs",
      name: "Dog"
    },
    {
      id: "188",
      name: "Cow"
    },
    {
      id: "bb0",
      name: "Lion"
    }
  ];


  droppedData: string;
  constructor() { }

  ngOnInit() {
  }

  dragEnd(event) {
    console.log('Element was dragged', event);
  }

}
