import { Component, OnInit } from '@angular/core';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

const now = new Date(2018, 11, 30);
@Component({
  selector: 'app-page4',
  templateUrl: './page4.component.html',
  styleUrls: ['./page4.component.css']
})
export class Page4Component implements OnInit {
  next: { year: number, month: number };
  next2next: { year: number, month: number };
  current: { year: number, month: number };

  model: NgbDateStruct = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
  tomorrow: NgbDateStruct = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };

  date: { year: number, month: number, day: number };
  dateT: Date;
  location: any;
  navigation = "none";
  outsideDays = "collapsed";
  showWeekNumbers = true;
  constructor() { }

  ngOnInit() {
   /*  this.current = { year: now.getFullYear(), month: now.getMonth() + 1 };
    this.next = { year: now.getFullYear(), month: now.getMonth() + 1 };
    this.next2next = { year: now.getFullYear(), month: now.getMonth() + 1 };

    console.log(this.current, this.next, this.next2next);
    if (this.current.month == 12) {
      //console.log(this.next);
      this.next.year = this.current.year + 1;
      this.next.month = 1;
      this.next2next.year = this.current.year + 1;
      this.next2next.month = 2;
    }
    else if (this.current.month == 11) {
      //console.log(this.next);
      this.next.year = this.current.year;
      this.next.month = this.current.month + 1;
      this.next2next.year = this.current.year + 1;
      this.next2next.month = 1;
    }
    else {
      //console.log(this.current);
      this.next.year = this.current.year;
      this.next.month = this.current.month + 1;
      this.next2next.year = this.current.year;
      this.next2next.month = this.current.month + 2;
      //console.log(this.next);
    } */
    this.selectToday();

    console.log();
  }

  selectToday() {
    this.model = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };

    this.current = { year: now.getFullYear(), month: now.getMonth() + 1 };
    this.next = { year: now.getFullYear(), month: now.getMonth() + 1 };
    this.next2next = { year: now.getFullYear(), month: now.getMonth() + 1 };

    if (this.current.month == 12) {
      //console.log(this.next);
      this.next.year = this.current.year + 1;
      this.next.month = 1;
      this.next2next.year = this.current.year + 1;
      this.next2next.month = 2;
    }
    else if (this.current.month == 11) {
      //console.log(this.next);
      this.next.year = this.current.year;
      this.next.month = this.current.month + 1;
      this.next2next.year = this.current.year + 1;
      this.next2next.month = 1;
    }
    else {
      //console.log(this.current);
      this.next.year = this.current.year;
      this.next.month = this.current.month + 1;
      this.next2next.month = this.current.month + 2;
      //console.log(this.next);
    }
    this.onDateChange(this.model);
  }

  selectTomorrow() {
    this.getTomorrow();
    this.model = this.tomorrow;
    this.onDateChange(this.model);
    console.log(this.tomorrow);

    if (this.tomorrow.month == 12) {
      //console.log(this.next);
        this.current.year = this.tomorrow.year;
        this.current.month = this.tomorrow.month;
        this.next.year = this.tomorrow.year + 1;
        this.next.month = 1;
        this.next2next.year = this.tomorrow.year + 1;
        this.next2next.month = 2;
    }
    else if (this.tomorrow.month == 11) {
      //console.log(this.next);
      this.current.year = this.tomorrow.year;
      this.current.month = this.tomorrow.month;
      this.next.year = this.tomorrow.year;
      this.next.month = this.tomorrow.month + 1;
      this.next2next.year = this.tomorrow.year + 1;
      this.next2next.month = 1;
    }
    else {
      //console.log(this.current);
      this.current.year = this.tomorrow.year;
      this.current.month = this.tomorrow.month;
      this.next.year = this.tomorrow.year;
      this.next.month = this.tomorrow.month + 1;
      this.next2next.year = this.tomorrow.year;
      this.next2next.month = this.tomorrow.month + 2;
      //console.log(this.next);
    }

    console.log(this.current, this.next, this.next2next);

  }

  getTomorrow() {
    let last = new Date(now.getFullYear(), now.getMonth() + 1, 0);
    if (last.getDate() == now.getDate()) {
      if (now.getMonth() + 1 == 12)
        this.tomorrow = {
          year: now.getFullYear() + 1,
          month: 1,
          day: 1
        };
      else
        this.tomorrow = {
          year: now.getFullYear(),
          month: now.getMonth() + 2,
          day: 1
        };
    }
    else
      this.tomorrow = {
        year: now.getFullYear(),
        month: now.getMonth() + 1,
        day: now.getDate() + 1
      };

    console.log(this.tomorrow);
  }

  gotoPrev() {
    //console.log(this.current);console.log(this.next);
    if (this.current.month == 1) {
      this.current.month = 12;
      this.current.year--;
      this.next.month--;
      this.next2next.month--;
    }
    else if (this.next.month == 1) {
      this.next.month = 12;
      this.next.year--;
      this.current.month--;
      this.next2next.month--;
    }
    else if (this.next2next.month == 1) {
      this.next2next.month = 12;
      this.next2next.year--;
      this.next.month--;
      this.current.month--;
    }
    else {
      this.next.month--;
      this.current.month--;
      this.next2next.month--;
    }

    //console.log(this.current);console.log(this.next);
  }


  gotoNext() {
    if (this.next.month == 12) {
      this.next.month = 1;
      this.next.year++;
      this.current.month++;
      this.next2next.month++;
    }
    else if (this.current.month == 12) {
      this.current.month = 1;
      this.current.year++;
      this.next.month++;
      this.next2next.month++;
    }
    else if (this.next2next.month == 12) {
      this.next2next.month = 1;
      this.next2next.year++;
      this.next.month++;
      this.current.month++;
    }
    else {
      this.current.month++;
      this.next.month++;
      this.next2next.month++;
    }
  }

  isToday(date: NgbDateStruct) {
    if (date.year == now.getFullYear() && date.month == now.getMonth() + 1 && date.day == now.getDate())
      return true;

    return false;
  }

  onDateChange(date: NgbDateStruct) {
    console.log("Selected Date:");
    console.log(date);
  }


}
