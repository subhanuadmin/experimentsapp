import { Component, OnInit } from '@angular/core';
//import { $ } from 'protractor';

import * as $ from 'jquery/dist/jquery';
import { HttpClient, HttpParams } from '@angular/common/http';
import { SomeServiceService } from '../../services/some-service.service';

export interface Reminder {
  id?: string;
  name: string;
  date: string;
  startTime: string;
  endTime: string;
  recurringId: string;
  recurringDate: RecurringD;
}

export interface RecurringD {
  startDate: string;
  endDate: string;
}

@Component({
  selector: 'app-page5',
  templateUrl: './page5.component.html',
  styleUrls: ['./page5.component.css']
})

export class Page5Component implements OnInit {

  reminder: Reminder = {
    name: "",
    date: "",
    startTime: "",
    endTime: "",
    recurringId: "",
    recurringDate: {
      startDate: "",
      endDate: ""
    }
  };
  reminderArr: Reminder[] = [];
  recurringOptionsArr = [
    {
      name: "Doesn't repeat",
      value: "no"
    },
    {
      name: "Daily",
      value: "daily"
    },
    {
      name: "Weekdays (Mon-Sat)",
      value: "weekdays"
    },
    {
      name: "Weekly",
      value: "weekly"
    },
    {
      name: "Monthly",
      value: "monthly"
    }
  ];

  recurringType: string;
  custom: any = {
    customNum: 0,
    customType: 'day'
  };
  customWeekdayArr: string[] = [];
  response: any;
  timeSoren;
  timeSarva; 
  timeMette;
  timeMichelle;
  timeStart;
  stylistsArr: any[];

  constructor(private _someService: SomeServiceService) { }

  ngOnInit() {
    $('.weekdays').prop('checked', false);

    var d = new Date();

    var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var weekdays = ["Sundays", "Mondays", "Tuesdays", "Wednesdays", "Thursdays", "Fridays", "Saturdays"];

    console.log(d.getMonth(), d.getFullYear());
    let weeknumber = this.getWeeksInMonth(d.getMonth(), d.getFullYear());
    console.log(weeknumber);

    var week;
    for (let w = 0; w < weeknumber.length; w++) {
      if (d.getDate() >= weeknumber[w].start && d.getDate() <= weeknumber[w].end) {
        switch (w + 1) {
          case 1: week = w + 1 + "st "; break;
          case 2: week = w + 1 + "nd "; break;
          case 3: week = w + 1 + "rd "; break;
          default: week = w + 1 + "th "; break;
        }

        break;
      }
    }

    for (let r = 3; r < this.recurringOptionsArr.length; r++) {
      if (this.recurringOptionsArr[r].value == "weekly")
        this.recurringOptionsArr[r].name = "Weekly, on " + weekdays[d.getDay()];
      else if (this.recurringOptionsArr[r].value == "monthly") {
        console.log(week);
        if (week.substr(0, 1) == '5')
          this.recurringOptionsArr[r].name = "Monthly, on last " + weekdays[d.getDay()];
        else
          this.recurringOptionsArr[r].name = "Monthly, on " + week + weekdays[d.getDay()];
      }


    }
  }


  getWeeksInMonth(month, year) {
    //console.log(month);
    var weeks = [],
      firstDate = new Date(year, month, 1),
      lastDate = new Date(year, month + 1, 0),
      numDays = lastDate.getDate();
    //console.log(firstDate, firstDate.getDay(), lastDate);
    var start = 1;
    var end = 7 - firstDate.getDay() + 1;
    while (start <= numDays) {
      weeks.push({ start: start, end: end });
      start = end + 1;
      end = end + 7;

      if (end > numDays)
        end = numDays;
    }
    //console.log(weeks);
    return weeks;
  }

  onSubmit() {
    console.log(this.reminder);
    var startYear = parseInt(this.reminder.date.substr(0, 4));
    var startMonth = parseInt(this.reminder.date.substr(5, 2));
    var startDay = parseInt(this.reminder.date.substr(8, 2));

    var endYear = parseInt(this.reminder.recurringDate.endDate.substr(0, 4));
    var endMonth = parseInt(this.reminder.recurringDate.endDate.substr(5, 2));
    var endDay = parseInt(this.reminder.recurringDate.endDate.substr(8, 2));

    var weekdays = {
      "sun": 0,
      "mon": 1,
      "tue": 2,
      "wed": 3,
      "thur": 4,
      "fri": 5,
      "sat": 6,
    }


    var startDate = new Date(startYear, startMonth - 1, startDay);
    //var lastDate = new Date(endYear, endMonth, 0);

    //console.log(lastDate.getDate());
    console.log(this.recurringType);

    if (this.recurringType == 'daily') {
      var lastDay = new Date(startYear, startMonth, 0);
      console.log(lastDay.getDate());

      while (startYear != endYear || startMonth != endMonth || startDay != endDay) {
        var lastDay = new Date(startYear, startMonth, 0);

        this.reminderArr.push({
          name: this.reminder.name,
          date: startYear + "-" + startMonth + "-" + startDay,
          startTime: this.reminder.startTime,
          endTime: this.reminder.endTime,
          recurringId: "",
          recurringDate: {
            startDate: this.reminder.date,
            endDate: this.reminder.recurringDate.endDate
          }
        });

        if (startDay < lastDay.getDate()) {
          startDay++;
        }
        else if (startDay == lastDay.getDate() && startMonth != 12) {
          startDay = 1;
          ++startMonth;
        }
        else if (startDay == lastDay.getDate() && startMonth == 12) {
          startDay = 1;
          startMonth = 1;
          ++startYear;
        }
      }

      this.reminderArr.push({
        name: this.reminder.name,
        date: endYear + "-" + endMonth + "-" + endDay,
        startTime: this.reminder.startTime,
        endTime: this.reminder.endTime,
        recurringId: "",
        recurringDate: {
          startDate: this.reminder.date,
          endDate: this.reminder.recurringDate.endDate
        }
      });

    }
    else if (this.recurringType == 'weekdays') {
      while (startYear != endYear || startMonth != endMonth || startDay != endDay) {
        var lastDay = new Date(startYear, startMonth, 0);
        var curD = new Date(startYear, startMonth - 1, startDay);

        //console.log(lastDay,curD, curD.getDay());
        if (curD.getDay() != 0)
          this.reminderArr.push({
            name: this.reminder.name,
            date: startYear + "-" + startMonth + "-" + startDay,
            startTime: this.reminder.startTime,
            endTime: this.reminder.endTime,
            recurringId: "",
            recurringDate: {
              startDate: this.reminder.date,
              endDate: this.reminder.recurringDate.endDate
            }
          });

        if (startDay < lastDay.getDate()) {
          startDay++;
        }
        else if (startDay == lastDay.getDate() && startMonth != 12) {
          startDay = 1;
          ++startMonth;
        }
        else if (startDay == lastDay.getDate() && startMonth == 12) {
          startDay = 1;
          startMonth = 1;
          ++startYear;
        }
      }

      this.reminderArr.push({
        name: this.reminder.name,
        date: endYear + "-" + endMonth + "-" + endDay,
        startTime: this.reminder.startTime,
        endTime: this.reminder.endTime,
        recurringId: "",
        recurringDate: {
          startDate: this.reminder.date,
          endDate: this.reminder.recurringDate.endDate
        }
      });
    }
    else if (this.recurringType == 'weekly') {
      var constWeekday = new Date(startYear, startMonth - 1, startDay);
      var lastD = new Date(endYear, endMonth - 1, endDay);
      console.log(lastD);
      if (lastD.getDay() == constWeekday.getDay())
        this.reminderArr.push({
          name: this.reminder.name,
          date: endYear + "-" + endMonth + "-" + endDay,
          startTime: this.reminder.startTime,
          endTime: this.reminder.endTime,
          recurringId: "",
          recurringDate: {
            startDate: this.reminder.date,
            endDate: this.reminder.recurringDate.endDate
          }
        });

      while (startYear != endYear || startMonth != endMonth || startDay != endDay) {
        var lastDay = new Date(startYear, startMonth, 0);
        var curD = new Date(startYear, startMonth - 1, startDay);

        //console.log(lastDay,curD, curD.getDay(), curD.getDay() , constWeekday.getDay());
        if (curD.getDay() == constWeekday.getDay())
          this.reminderArr.push({
            name: this.reminder.name,
            date: startYear + "-" + startMonth + "-" + startDay,
            startTime: this.reminder.startTime,
            endTime: this.reminder.endTime,
            recurringId: "",
            recurringDate: {
              startDate: this.reminder.date,
              endDate: this.reminder.recurringDate.endDate
            }
          });

        if (startDay < lastDay.getDate()) {
          startDay++;
        }
        else if (startDay == lastDay.getDate() && startMonth != 12) {
          startDay = 1;
          ++startMonth;
        }
        else if (startDay == lastDay.getDate() && startMonth == 12) {
          startDay = 1;
          startMonth = 1;
          ++startYear;
        }
      }
    }
    else if (this.recurringType == 'monthly') {
      var constWeekday = new Date(startYear, startMonth - 1, startDay);
      var startWeeknumberArr = this.getWeeksInMonth(startMonth - 1, startYear);
      var lastWeeknumberArr = this.getWeeksInMonth(endMonth - 1, endYear);
      var lastD = new Date(endYear, endMonth - 1, endDay);
      var weeknumber;
      for (let w = 0; w < startWeeknumberArr.length; w++) {
        if (startDay >= startWeeknumberArr[w].start && startDay <= startWeeknumberArr[w].end) {
          weeknumber = w;
          break;
        }
      }

      console.log(weeknumber);
      let c = -1;
      while (startYear != endYear || startMonth < endMonth) {
        var lastDay = new Date(startYear, startMonth, 0);
        var curWeeknumberArr = this.getWeeksInMonth(startMonth - 1, startYear);
        console.log(curWeeknumberArr);

        if (weeknumber <= 3) {
          for (let i = curWeeknumberArr[weeknumber].start; i <= curWeeknumberArr[weeknumber].end; i++) {
            var curD = new Date(startYear, startMonth - 1, i);
            if (constWeekday.getDay() == curD.getDay())
              this.reminderArr.push({
                name: this.reminder.name,
                date: startYear + "-" + startMonth + "-" + i,
                startTime: this.reminder.startTime,
                endTime: this.reminder.endTime,
                recurringId: "",
                recurringDate: {
                  startDate: this.reminder.date,
                  endDate: this.reminder.recurringDate.endDate
                }
              });
          }
        }
        else {
          let found: boolean = false;
          for (let m = curWeeknumberArr.length - 1; m >= 3; m--) {
            for (let i = curWeeknumberArr[m].start; i <= curWeeknumberArr[m].end; i++) {
              var curD = new Date(startYear, startMonth - 1, i);
              if (constWeekday.getDay() == curD.getDay()) {
                this.reminderArr.push({
                  name: this.reminder.name,
                  date: startYear + "-" + startMonth + "-" + i,
                  startTime: this.reminder.startTime,
                  endTime: this.reminder.endTime,
                  recurringId: "",
                  recurringDate: {
                    startDate: this.reminder.date,
                    endDate: this.reminder.recurringDate.endDate
                  }
                });
                found = true;
                break;
              }
            }

            if (found == true)
              break;
          }
        }

        if (startMonth < 12) {
          ++startMonth;
        }
        else if (startMonth == 12) {
          startMonth = 1;
          ++startYear;
        }

        console.log(startYear, startMonth, endYear, endMonth);
        /* ++c;
        if(c > 10)
        break; */

      }

      let tempFound: boolean = false;
      var tempCurWeeknumberArr = this.getWeeksInMonth(startMonth - 1, startYear);
      for (let m = tempCurWeeknumberArr.length - 1; m >= 3; m--) {
        for (let i = tempCurWeeknumberArr[m].start; i <= tempCurWeeknumberArr[m].end; i++) {
          var curD = new Date(startYear, startMonth - 1, i);
          if (constWeekday.getDay() == curD.getDay()) {
            if (i <= endDay) {
              this.reminderArr.push({
                name: this.reminder.name,
                date: startYear + "-" + startMonth + "-" + i,
                startTime: this.reminder.startTime,
                endTime: this.reminder.endTime,
                recurringId: "",
                recurringDate: {
                  startDate: this.reminder.date,
                  endDate: this.reminder.recurringDate.endDate
                }
              });
            }
            tempFound = true;
            break;
          }
        }

        if (tempFound == true)
          break;
      }




    }
    /* else if (this.recurringType == 'monthly') {
      var constWeekday = new Date(startYear, startMonth - 1, startDay);
      var startWeeknumberArr = this.getWeeksInMonth(startMonth - 1, startYear);
      var lastWeeknumberArr = this.getWeeksInMonth(endMonth - 1, endYear);
      var lastD = new Date(endYear, endMonth - 1, endDay);
      var weeknumber;
      for (let w = 0; w < startWeeknumberArr.length; w++) {
        if (startDay >= startWeeknumberArr[w].start && startDay <= startWeeknumberArr[w].end) {
          weeknumber = w;
          break;
        }
      }

      console.log(weeknumber);
      while (startYear != endYear || startMonth != endMonth || startDay != endDay) {
        var lastDay = new Date(startYear, startMonth, 0);
        var curD = new Date(startYear, startMonth - 1, startDay);
        var curWeeknumberArr = this.getWeeksInMonth(startMonth - 1, startYear);


        //console.log(lastDay,curD, curD.getDay());
        //console.log(curD, curWeeknumberArr,constWeekday.getDay() , curD.getDay() , startDay , curWeeknumberArr[weeknumber].start , startDay , curWeeknumberArr[weeknumber].end);
        if (constWeekday.getDay() == curD.getDay() && startDay >= curWeeknumberArr[weeknumber].start && startDay <= curWeeknumberArr[weeknumber].end) {
          this.reminderArr.push({
            name: this.reminder.name,
            date: startYear + "-" + startMonth + "-" + startDay,
            startTime: this.reminder.startTime,
            endTime: this.reminder.endTime,
            recurringId: "",
            recurringDate: {
              startDate: this.reminder.date,
              endDate: this.reminder.recurringDate.endDate
            }
          });
        }


        if (startDay < lastDay.getDate()) {
          startDay++;
        }
        else if (startDay == lastDay.getDate() && startMonth != 12) {
          startDay = 1;
          ++startMonth;
        }
        else if (startDay == lastDay.getDate() && startMonth == 12) {
          startDay = 1;
          startMonth = 1;
          ++startYear;
        }
      }


      if (constWeekday.getDay() == lastD.getDay() && startDay >= lastWeeknumberArr[weeknumber].start && startDay <= lastWeeknumberArr[weeknumber].end) {
        this.reminderArr.push({
          name: this.reminder.name,
          date: startYear + "-" + startMonth + "-" + startDay,
          startTime: this.reminder.startTime,
          endTime: this.reminder.endTime,
          recurringId: "",
          recurringDate: {
            startDate: this.reminder.date,
            endDate: this.reminder.recurringDate.endDate
          }
        });
      }

    } */
    /* else if (this.recurringType == 'yearly') {
      var constWeekday = new Date(startYear, startMonth - 1, startDay);
      var constWeeknumberArr = this.getWeeksInMonth(startMonth - 1, startYear);
      let weeknumber;

      for (let w = 0; w < constWeeknumberArr.length; w++) {
        if (startDay >= constWeeknumberArr[w].start && startDay <= constWeeknumberArr[w].end) {
          weeknumber = w;
          break;
        }
      }

      while (startYear <= endYear) {
        var curWeeknumberArr = this.getWeeksInMonth(startMonth - 1, startYear);
        console.log(curWeeknumberArr);

        for (let wn = curWeeknumberArr[weeknumber].start; wn <= curWeeknumberArr[weeknumber].end; wn++) {
          var curWeekday = new Date(startYear, startMonth - 1, wn);
          console.log(wn, startYear, constWeekday.getDay(), curWeekday.getDay());

          if (constWeekday.getDay() == curWeekday.getDay()) {
            if (startYear == endYear) {
              if (wn <= endDay)
                this.reminderArr.push({
                  name: this.reminder.name,
                  date: startYear + "-" + startMonth + "-" + wn,
                  startTime: this.reminder.startTime,
                  endTime: this.reminder.endTime,
                  recurringId: "",
                  recurringDate: {
                    startDate: this.reminder.date,
                    endDate: this.reminder.recurringDate.endDate
                  }
                });
            }
            else
              this.reminderArr.push({
                name: this.reminder.name,
                date: startYear + "-" + startMonth + "-" + wn,
                startTime: this.reminder.startTime,
                endTime: this.reminder.endTime,
                recurringId: "",
                recurringDate: {
                  startDate: this.reminder.date,
                  endDate: this.reminder.recurringDate.endDate
                }
              });

            ++startYear;
            break;
          }
        }
      }
    }
    else if (this.recurringType == 'custom') {
      if (this.custom.customType == 'daily') {
        while (startYear != endYear || startMonth != endMonth || startDay <= endDay) {
          var lastDay = new Date(startYear, startMonth, 0);

          if (startDay == 0) {
            var tempDay = new Date(startYear, startMonth - 1, 0);
            this.reminderArr.push({
              name: this.reminder.name,
              date: startYear + "-" + (startMonth - 1) + "-" + tempDay.getDate(),
              startTime: this.reminder.startTime,
              endTime: this.reminder.endTime,
              recurringId: "",
              recurringDate: {
                startDate: this.reminder.date,
                endDate: this.reminder.recurringDate.endDate
              }
            });
          }
          else
            this.reminderArr.push({
              name: this.reminder.name,
              date: startYear + "-" + startMonth + "-" + startDay,
              startTime: this.reminder.startTime,
              endTime: this.reminder.endTime,
              recurringId: "",
              recurringDate: {
                startDate: this.reminder.date,
                endDate: this.reminder.recurringDate.endDate
              }
            });

          console.log((startDay + this.custom.customNum), lastDay.getDate());
          if ((startDay + this.custom.customNum) < lastDay.getDate()) {
            startDay += this.custom.customNum;
          }
          else if ((startDay + this.custom.customNum) >= lastDay.getDate() && startMonth != 12) {
            let diff = (startDay + this.custom.customNum) - lastDay.getDate();

            if (diff == 0) {
              startDay = lastDay.getDate();
            }
            else {
              startDay = diff;
              ++startMonth;
            }
          }
          else if ((startDay + this.custom.customNum) >= lastDay.getDate() && startMonth == 12) {
            let diff1 = (startDay + this.custom.customNum) - lastDay.getDate();

            if (diff1 == 0) {
              startDay = lastDay.getDate();
            }
            else {
              startDay = diff1;
              startMonth = 1;
              ++startYear;
            }
          }

          console.log(startYear, endYear, startMonth, endMonth, startDay, endDay);
        }
      }
      else if (this.custom.customType == 'monthly') {
        while (startYear != endYear || startMonth < endMonth) {
          var lastDay = new Date(startYear, startMonth, 0);

          this.reminderArr.push({
            name: this.reminder.name,
            date: startYear + "-" + startMonth + "-" + startDay,
            startTime: this.reminder.startTime,
            endTime: this.reminder.endTime,
            recurringId: "",
            recurringDate: {
              startDate: this.reminder.date,
              endDate: this.reminder.recurringDate.endDate
            }
          });

          //console.log((startDay + this.custom.customNum), lastDay.getDate());
          if ((startMonth + this.custom.customNum) < 12) {
            startMonth += this.custom.customNum;
          }
          else if ((startMonth + this.custom.customNum) >= 12) {
            let diff = (startMonth + this.custom.customNum) - 12;

            if (diff == 0) {
              startMonth = 12;
            }
            else {
              startMonth = diff;
              ++startYear;
            }
          }

          console.log(startYear, endYear, startMonth, endMonth, startDay, endDay);
        }

        if (startYear == endYear && startMonth == endMonth) {
          if (startDay <= endDay)
            this.reminderArr.push({
              name: this.reminder.name,
              date: startYear + "-" + startMonth + "-" + startDay,
              startTime: this.reminder.startTime,
              endTime: this.reminder.endTime,
              recurringId: "",
              recurringDate: {
                startDate: this.reminder.date,
                endDate: this.reminder.recurringDate.endDate
              }
            });
        }
      }
      else if (this.custom.customType == 'weekly') {
        let weekNumberArr = this.getWeeksInMonth(startMonth - 1, startYear);
        let weeknumber;
        let c = -1;
        for (let w = 0; w < weekNumberArr.length; w++) {
          //console.log(weekNumberArr[w].start, startDay, weekNumberArr[w].end, startDay);
          if (startDay >= weekNumberArr[w].start && startDay <= weekNumberArr[w].end) {
            weeknumber = w;
            break;
          }
        }

        while (startYear != endYear || startMonth <= endMonth) {
          var lastDay = new Date(startYear, startMonth, 0);
          weekNumberArr = this.getWeeksInMonth(startMonth - 1, startYear);
          console.log(weekNumberArr, weeknumber, weekNumberArr[weeknumber].start);

          for (let w = weekNumberArr[weeknumber].start; w <= weekNumberArr[weeknumber].end; w++) {
            let weekday = new Date(startYear, startMonth-1, w);
            console.log(weekday);

            for (let d = 0; d < this.customWeekdayArr.length; d++) {
              console.log(weekdays[this.customWeekdayArr[d]], weekday.getDay());
              if (weekdays[this.customWeekdayArr[d]] == weekday.getDay())
                this.reminderArr.push({
                  name: this.reminder.name,
                  date: startYear + "-" + startMonth + "-" + w,
                  startTime: this.reminder.startTime,
                  endTime: this.reminder.endTime,
                  recurringId: "",
                  recurringDate: {
                    startDate: this.reminder.date,
                    endDate: this.reminder.recurringDate.endDate
                  }
                });
            }
          }

          console.log('Weeknum:'+(weeknumber + 1 + this.custom.customNum), weekNumberArr.length, weekNumberArr);
          if ((weeknumber + 1 + this.custom.customNum) < weekNumberArr.length) {
            weeknumber += this.custom.customNum + 1;
          }
          else if ((weeknumber + 1 + this.custom.customNum) >= weekNumberArr.length && startMonth != 12) {
            let diff = (weeknumber + 1 + this.custom.customNum) - weekNumberArr.length;

            if (diff == 0) {
              weeknumber = weekNumberArr.length-1;
            }
            else {
              weeknumber = diff;
              ++startMonth;
            }
          }
          else if ((weeknumber + 1 + this.custom.customNum) >= weekNumberArr.length && startMonth == 12) {
            let diff = (weeknumber + 1 + this.custom.customNum) - weekNumberArr.length;

            if (diff == 0) {
              weeknumber = weekNumberArr.length-1;
            }
            else {
              weeknumber = diff;
              startMonth = 1;
              ++startYear
            }
          }

          console.log(startYear, startMonth, weeknumber);
        }
      }
    } */
  }

  onSelectRecurringOption(option) {
    console.log(option);
    //$('.table').css('color','#1db');
    if (option == 'custom')
      $('#exampleModal').addClass('show');
  }

  hideModal() {
    $('#exampleModal').removeClass('show');
  }

  selectWeekday(event) {
    var id = event.target.id;

    if (!$('#' + id).hasClass('checked')) {
      $('#' + id).css('background-color', '#1db');
      $('#' + id).addClass('checked');

      this.customWeekdayArr.push(id);
    }
    else {
      $('#' + id).css('background-color', '#e0e0e0');
      $('#' + id).removeClass('checked');

      var index = this.customWeekdayArr.indexOf(id);
      this.customWeekdayArr.splice(index, 1);
    }

    console.log(this.customWeekdayArr);
  }


  hover(event) {
    console.log("called");
    $('#event').css('box-shadow', '0px 0px 15px 0px #e0e0e0');
    $('#event').css('color', '#78d666');
  }


  callCloudFunc() {
    $(".progress").css('display','flex');
    $(".progress-bar").css('width','0%');
    this.stylistsArr = [
      {
        stylist: "BqYWKWzs4r8SyGQvyqH2",
        endTime: this.timeSoren
      },
      {
        stylist: "at5kjx5FqIxbnMys8w4q",
        endTime: this.timeSarva
      },
      {
        stylist: "nFI5hxfIePx240dmqR0R",
        endTime: this.timeMette
      },
      {
        stylist: "spxSj8UZem5uD0wL46EP",
        endTime: this.timeMichelle
      }
    ];

    this._someService.callCloudFunction(this.stylistsArr, this.timeStart)
      .toPromise()
      .then((res) => {
        console.log("Res: " + res);
        this.response = res;
        $(".progress-bar").css('width','100%');        
      })
      .catch((err) => {
        console.log(err);
        $(".progress-bar").css('width','100%');
      });

      
  }



}


