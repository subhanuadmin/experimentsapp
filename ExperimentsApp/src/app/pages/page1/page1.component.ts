import { Component, OnInit } from '@angular/core';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap/datepicker/ngb-date-struct';
declare var $: any;

const now = new Date();
@Component({
  selector: 'app-page1',
  templateUrl: './page1.component.html',
  styleUrls: ['./page1.component.css']
})
export class Page1Component implements OnInit {
  today: NgbDateStruct = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
  tomorrow: NgbDateStruct = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
  constructor() { }

  ngOnInit() {
  }

  changeColor() {
    $('#para').css('color', '#1db');
  }

  onDateSelect(date) {
    console.log(date);
    console.log();
    let last = new Date(date.year, date.month, 0);

    if (last.getDate() == date.day) {
      if (date.month == 12)
        this.tomorrow = {
          year: ++date.year,
          month: 1,
          day: 1
        };
      else
        this.tomorrow = {
          year: date.year,
          month: ++date.month,
          day: 1
        };
    }
    else
      this.tomorrow = {
        year: date.year,
        month: date.month,
        day: ++date.day
      };
  }

}
